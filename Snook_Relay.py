from tkinter import  *
from tkinter import messagebox
from datetime import *
import time
import pickle
from os.path import exists
import os
import signal
import cups
from PIL import ImageTk, Image
import lib4relay
shift_time = "09:00:00"
class checkboards():  
    def check_board(self, stack):
        try:
            lib4relay.get_all(stack)
            return True
        except:
            return False   
    def check_relay_state(self, stack, relay):
        try:
            mValue = lib4relay.get(stack, relay)
            return mValue
        except:
            return False    
    def Set_board(self, stack, switch):
        try:
            lib4relay.set_all(stack, switch)
            return True
        except:
            return False    
    def set_relay(self, stack, relay, switch):
        try:
            lib4relay.set(stack, relay, switch)
            return True
        except:
            return False

class Defaults():
    def save_entry(self, mVar, mValue):
        file_exists = exists("/home/pi/Snooker/storage")
        if file_exists:
            infile = open("/home/pi/Snooker/storage",'rb')
            var_dict = pickle.load(infile)
            infile.close()
            if mVar == "Cost":
                var_dict ['Cost'] =  mValue #store new cost
            elif mVar == "Shift":
                var_dict ['Shift'] = mValue #store new shift change time
            elif mVar == "File":
                var_dict ['File'] = mValue #store new filename
            outfile = open("/home/pi/Snooker/storage",'wb')
            pickle.dump(var_dict, outfile)
            outfile.close()
        else:
            var_dict = {"Cost": 0.00, "Shift": "09:00", "File": "default"}#setup defaults
            outfile = open("/home/pi/Snooker/storage",'wb')
            pickle.dump(var_dict, outfile)
            outfile.close()
            self.save_entry(mVar, mValue) 
    #Retrieve entries
    def retrieve_entry(self):
        file_exists = exists("/home/pi/Snooker/storage")
        if file_exists:            
            infile = open("/home/pi/Snooker/storage",'rb')
            var_dict = pickle.load(infile)
            return var_dict
        else:
            var_dict = {"Cost": 0.00, "Shift": "09:00", "File": "default"}#setup defaults           
            outfile = open("/home/pi/Snooker/storage",'wb')
            pickle.dump(var_dict, outfile)
            outfile.close()
            return var_dict 
        
class calculations():
    def find_diff (self, time_tuple_end, time_tuple_start):
        # need to covert to a datetime.time format (hour = , min = , s = )
        self.end_time = datetime(time_tuple_end.tm_year, time_tuple_end.tm_mon, time_tuple_end.tm_mday, time_tuple_end.tm_hour, time_tuple_end.tm_min)      
        self.start_time = datetime(time_tuple_start.tm_year, time_tuple_start.tm_mon, time_tuple_start.tm_mday, time_tuple_start.tm_hour, time_tuple_start.tm_min)
        self.my_val = self.end_time - self.start_time       
        self.dur_2 = self.my_val.total_seconds()
        self.dur_2 = self.dur_2 / 60
        self.tmp_used = int(self.dur_2)
        return self.tmp_used
    
class fileactions():         
    def get_file(self):
        global shift_time
        self.myValues = Defaults.retrieve_entry(self)
        shift_time = self.myValues.get("Shift")
        self.filename = self.myValues.get("File")
        if self.filename is None or self.filename == "default":
            self.create_file()
            self.get_file()   
    def append_data(self, table, start, stop, used, cost):
        self.get_file()
        self.my_file = open("/home/pi/Downloads/LogFiles/" + self.filename, "a")#continue with existing file
        #Data to write     table No             start time       end time    used time      cost of table
        self.file_write = table + "     " + start + "     " + stop + "     " + used + "     " + cost + "     " + "\r\n"
        self.my_file.write(self.file_write)# write data to file
        self.my_file.close()# close file       
    def create_file(self):
        time_tuple = time.localtime()
        today_str = time.strftime("%Y%B%d", time_tuple)#create string of 'yearmonthday' for file name
        my_file = open("/home/pi/Downloads/LogFiles/" + today_str, "a")#creates a new file marked today
        file_write = "File Created " + str(today_str) + "     " + "\r\n"
        my_file.write(file_write)# write data to file
        my_file.close()# close file
        newfilename = str(today_str)
        my_save = Defaults()
        my_save.save_entry("File", newfilename)        
    def read_file(self, filename):
        self.filename = filename
        self.get_file()
        self.my_file = open("/home/pi/Downloads/LogFiles/" + self.filename, "r")#open file for reading
        self.myfread = self.my_file.read()
        self.my_file.close()# close file
        return self.myfread # return file text

class popupwindow():
    def _init_(self):
        child_w=None
    def new_window(self, app, wintext):       
        self.child_w= Toplevel(app)
        self.child_w.geometry("500x150")
        self.myContainer1 = Frame(app)
        self.myContainer1.grid(column=1, row=1)
        self.mClass = Defaults()
        self.Values = self.mClass.retrieve_entry()
        self.value_var = StringVar()
        self.time1_var = IntVar()
        self.time2_var = IntVar()
        self.time3_var = IntVar()        
        if wintext == "cost":
            self.child_w.title("Set table cost")
            self.label_Value= Label(self.child_w, text= "Current cost per minute = " + str(self.Values.get("Cost")), font=('Helvetica 15'))
            self.label_Value.grid(row=1, column=1, sticky='w', padx=5, pady=5)
            self.label_inf= Label(self.child_w, text= "Enter new cost ", font=('Helvetica 15'))
            self.label_inf.grid(row=2, column=1, sticky='w', padx=5, pady=5)
            self.txt_cost= Entry(self.child_w, textvariable= self.value_var , font=('Helvetica 15'), width=5)
            self.txt_cost.grid(row=2, column=2, sticky='w', padx=5, pady=5)
            self.OKButton = Button(self.child_w, text="Save", command=self.okcostpress)
            self.OKButton.grid(column=1, row=3, padx=2, pady=2)
            self.CancelButton = Button(self.child_w, text="Cancel", command=self.cancelbuttonpress)
            self.CancelButton.grid(column=2, row=3, padx=2, pady=2)     
        if wintext == "time":
            self.child_w.title("Set new file time")
            self.label_Value= Label(self.child_w, text= "Current Time for File swap = " + str(self.Values.get("Shift")), font=('Helvetica 15'))
            self.label_Value.grid(row=1, column=1, sticky='w', padx=5, pady=5)
            self.spn_time1= sp = Spinbox(self.child_w, textvariable= self.time1_var, from_= 0, to = 23, width = 2)
            self.spn_time1.grid(row=1, column=2, sticky='w', padx=5, pady=5)
            self.spn_time2= sp = Spinbox(self.child_w, textvariable= self.time2_var, from_= 0, to = 59, width = 2)
            self.spn_time2.grid(row=1, column=3, sticky='w', padx=5, pady=5)
            self.spn_time3= sp = Spinbox(self.child_w, textvariable= self.time3_var, from_= 0, to = 59, width = 2)
            self.spn_time3.grid(row=1, column=4, sticky='w', padx=5, pady=5)
            self.OKButton = Button(self.child_w, text="Save", command=self.oktimepress)
            self.OKButton.grid(column=1, row=3, padx=2, pady=2)
            self.CancelButton = Button(self.child_w, text="Cancel", command=self.cancelbuttonpress)
            self.CancelButton.grid(column=2, row=3, padx=2, pady=2)           
        if wintext == "file":#not yet implemented
            self.child_w.title("Set new file path")
            self.label_Value= Label(self.child_w, text= "Current default file location = " + str(self.Values.get("File")), font=('Helvetica 15'))
            self.label_Value.grid(row=1, column=1, sticky='w', padx=5, pady=5)
            self.txt_File= Entry(self.child_w, textvariable= self.value_var, font=('Helvetica 15'), width=20)
            self.txt_File.grid(row=1, column=2, columnspan=3, sticky='w', padx=5, pady=5)
            self.OKButton = Button(self.child_w, text="Save")
            self.OKButton.grid(column=1, row=3, padx=2, pady=2)
            self.CancelButton = Button(self.child_w, text="Cancel", command=self.cancelbuttonpress)
            self.CancelButton.grid(column=2, row=3, padx=2, pady=2)        
    def okcostpress(self):
        my_def = Defaults()
        my_def.save_entry("Cost", self.value_var.get())
        self.child_w.destroy()       
    def oktimepress(self):
        Hrs = self.time1_var.get()
        Mins =  self.time2_var.get()
        Secs = self.time3_var.get()
        if Hrs <10: Hrs = "{0:0>2}".format(Hrs)    
        if Mins <10: Mins = "{0:0>2}".format(Mins)    
        if Secs <10: Secs = "{0:0>2}".format(Secs)    
        SaveString = str(Hrs) + ":" + str(Mins) + ":" + str(Secs)
        my_def = Defaults()
        my_def.save_entry("time", SaveString)
        global shift_time
        shift_time = SaveString
        self.child_w.destroy()       
    def cancelbuttonpress(self):
        self.child_w.destroy()

class clsviewfiles():
    def __init__(self):
        child_w=None       
    def create_window(self, app):
        self.child_w= Toplevel(app)
        self.child_w.geometry("700x600")
        self.myContainer1 = Frame(app)
        self.myContainer1.grid(column=1, row=1)       
        self.my_label = Label(self.child_w, text="View Daily Logs", fg='blue')
        self.my_text = Text(self.child_w, width=50, height=30)
        self.my_button = Button(self.child_w, text="Print", command=self._print_file)
        self.del_button = Button(self.child_w, text="Delete", command=self.delete_item)
        self.my_list = os.listdir("/home/pi/Downloads/LogFiles/")#get file names from directory
        self.my_listbox = Listbox(self.child_w)#bind box to mouseclick
        self.my_listbox.bind("<<ListboxSelect>>", self.open_file)
        self.fill_listbox()
        self.my_label.grid(column=0, row=0, padx=2, pady=2)
        self.my_listbox.grid(column=0, row=1, padx=2, pady=2)
        self.my_button.grid(column=0, row=2, padx=2, pady=2)
        self.del_button.grid(column=0, row=3, padx=2, pady=2)
        self.my_text.grid(column=1, row=0, rowspan=20, padx=2, pady=2)        
    def open_file(self, event):#open file in text
        try:
            self.my_text.delete(1.0, "end")
            self.view_file = self.my_listbox.get(self.my_listbox.curselection())
            self.view_file = "/home/pi/Downloads/LogFiles/" + self.view_file
            self.view_file = open(self.view_file)
            self.my_data = self.view_file.read()
            self.my_text.insert("end", self.my_data)
            self.view_file.close()
        except ValueError as Argument:
            messagebox.showinfo("Error", Argument)
    def _print_file(self):
        try:
            conn = cups.Connection()# open connection
            printer = conn.getDefault()# get default printer
            file = self.my_listbox.get(self.my_listbox.curselection())# get the filename to print
            file = "/home/pi/Downloads/LogFiles/" + file# extend to full file path
            conn.printFile (printer, file, "Daily Report ", {})# send to default printer
        except:
            messagebox.showinfo("WARNING","No Printers are installed")            
    def fill_listbox(self):
        try:
            self.my_listbox.delete(0, "end")
            self.my_list = os.listdir("/home/pi/Downloads/LogFiles/")#get file names from directory
            i = 1
            for file in self.my_list:#fill list box with file names
                self.my_listbox.insert(i, file)
                i +=1
        except ValueError as Argument:
            messagebox.showinfo("Error", Argument)
    def delete_item(self):
        try:
            self.rem_file = self.my_listbox.get(self.my_listbox.curselection())
            self.rem_file = "/home/pi/Downloads/LogFiles/" + self.rem_file
            os.remove(self.rem_file)
            self.fill_listbox()
        except ValueError as Argument:
            messagebox.showinfo("Error", Argument)        
class MenuBar(Menu, Defaults):
    def __init__(self, parent):
        Menu.__init__(self, parent)
        file = Menu(self, tearoff=False)
        file.add_command(label="View Logs", command=self.create_window)  
        file.add_command(label="Change Table cost", command=self.show_popup1)  
        file.add_command(label="New Shift Time", command=self.show_popup2)    
        file.add_separator()
        file.add_command(label="Exit", command=self._quit)
        self.add_cascade(label="Actions", menu=file)        
        help = Menu(self, tearoff=0)  
        help.add_command(label="About", command=self.about)  
        self.add_cascade(label="Help", menu=help)    
    def _quit(self):  #Close window and destroy instance
        app.destroy()       
    def create_window(self):
        mWin = clsviewfiles()
        mWin.create_window(app)       
    def show_popup1(self):
        mWin = popupwindow()
        mWin.new_window(app, "cost")        
    def show_popup2(self):
        mWin = popupwindow()
        mWin.new_window(app, "time")       
    def about(self):
        messagebox.showinfo('Snooker Lighting Software', 'Simple Lighting software for 12 Tables running on a raspberry Pi')
        
class table_frame(Frame, calculations, fileactions, Defaults):
    class_counter = 0    
    def __init__(self, parent):
        super().__init__()
        self.myParent = parent   
        self.myContainer1 = Frame(parent)
        self.myContainer1.grid(column=1, row=1)
        #setup required tkinter variables
        self.varBool= BooleanVar()
        self.varSta = StringVar()
        self.varSto = StringVar()
        self.varUsed = IntVar()
        self.varCost = DoubleVar()
        self.varTable = StringVar()
        self.varLast = StringVar()
        self.varSta.set("")
        self.varSto.set("")#clear the stop label
        self.varUsed.set("")#clear the used label
        self.varCost.set("")#clear the cost label
        self.varBool.set(False)
        table_frame.class_counter+=1
        self.varRef = table_frame.class_counter
        self.varTable.set("Table No " + str(self.varRef))
        self.mval = False
        self.time_tuple_start = time.localtime()
        #load images for table in or out of use
        self.image1 = Image.open("/home/pi/Snooker/out.png")
        self.image1 = self.image1.resize((315, 300), Image.ANTIALIAS)
        self.image1= ImageTk.PhotoImage(self.image1)
        self.image2 = Image.open("/home/pi/Snooker/in.png")
        self.image2 = self.image2.resize((315, 300), Image.ANTIALIAS)
        self.image2= ImageTk.PhotoImage(self.image2)
        #Create widgets
        lbl_tableNo = Label(self, textvariable=self.varTable, bg="White", width=30)
        self.pic_button = Button(self, height=315, width=300, image=self.image1, command=self.butt_pressed)        
        start_lbl = Label(self, text="Start Time")
        stop_lbl = Label(self, text="End time")
        used_lbl = Label(self, text="Elapsed time")
        cost_lbl = Label(self, text="Cost")
        start_box = Label(self, textvariable=self.varSta, bg="White", width=10)
        stop_box = Label(self, textvariable=self.varSto, bg="White", width=10)
        used_box = Label(self, textvariable=self.varUsed, bg="White", width=10)
        Cost_box = Label(self, textvariable=self.varCost, bg="White", width=10)
        lbl_last = Label(self, textvariable=self.varLast, bg="White", width=30)
        # Layout widgets
        lbl_tableNo.grid(column=0, row=0, columnspan=2, rowspan=2, padx=2, pady=2)
        self.pic_button.grid(column=0, row=2, columnspan=2, rowspan=2, padx=2, pady=2)
        start_lbl.grid(column=0, row=4, sticky="w", padx=2, pady=2)
        start_box.grid(column=1, row=4, padx=2, pady=2)
        stop_lbl.grid(column=0, row=5, sticky="w", padx=2, pady=2)
        stop_box.grid(column=1, row=5, padx=2, pady=2)
        used_lbl.grid(column=0, row=6, sticky="w", padx=2, pady=2)
        used_box.grid(column=1, row=6, padx=2, pady=2)
        cost_lbl.grid(column=0, row=7, sticky="w", padx=2, pady=2)
        Cost_box.grid(column=1, row=7, padx=2, pady=2)
        lbl_last.grid(column=0, row=8, columnspan=2, padx=2, pady=2)
        self.retrieve_def()             
    def retrieve_def(self):
        global shift_time
        my_def = Defaults()
        self.myValues = my_def.retrieve_entry()
        self.T_Cost = self.myValues.get("Cost")
        shift_time = self.myValues.get("Shift")        
    def butt_pressed (self):
        if self.varRef <=4:
            self.rLevel = 0
            self.relay = self.varRef
        elif self.varRef >4 and self.varRef <=8:
            self.rLevel =1
            self.relay = self.varRef-4
        elif self.varRef >8 and self.varRef <=12:
            self.rLevel =2
            self.relay = self.varRef-8        
        if self.varBool.get() == False: 
            self.varBool.set(True)#reset flag
            self.mRet = checkboards()
            self.mVal = self.mRet.set_relay(self.rLevel, self.relay, 1)
            print(self.mVal)
            if self.mVal == False:
               self.varBool.set(False) 
               messagebox.showinfo('Snooker Lighting Software', 'Card or relay error, cannot set')
               return
            self.time_tuple_start = time.localtime() # time structure
            self.time_string = time.strftime("%H:%M", self.time_tuple_start)#put into hour and minute format
            self.varSta.set(self.time_string)#set the start label
            self.varSto.set("")#clear the stop label
            self.varUsed.set("")#clear the used label
            self.varCost.set("")#clear the cost label
            self.pic_button.configure(image=self.image2)            
        else:
            self.varBool.set(False)#reset flag
            self.mRet = checkboards()
            self.mRet.set_relay(self.rLevel, self.relay, 0)
            self.calc_time_used("butt")
            self.varCost.set("£{:,.2f}".format(self.varUsed.get()*self.T_Cost))
            self.tmp_cost = self.varUsed.get()*self.T_Cost
            fileactions.append_data(self, str(self.varRef), str(self.varSta.get()), str(self.varSto.get()), str(self.tmp_used), str("£{:,.2f}".format(self.tmp_cost)))#function to print details to daily file
            self.varLast.set(str(self.varRef) + "  " + str(self.varSta.get()) + "  " + str(self.varSto.get()) + "  " + str(self.tmp_used) + "  " + str("£{:,.2f}".format(self.tmp_cost)))
            self.pic_button.configure(image=self.image1)       
    def calc_time_used(self, caller): 
        self.time_tuple_end = time.localtime() # time structure
        self.time_string = time.strftime("%H:%M", self.time_tuple_end)#put into hour and minute format
        if caller == "butt":
            self.varSto.set(self.time_string)#set the end label
        self.time_used = calculations.find_diff(self, self.time_tuple_end, self.time_tuple_start)#function to work out time and cost and set the labels
        self.varUsed.set(self.time_used)
         
class main_view(Tk, table_frame):
    def __init__(self):
        super().__init__()
        #initialise 12 tables
        for index in range(12):
           frame = table_frame(self)
           frame.grid(row=(0 if index  <6 else 1), column=(index if index <6 else index - 6), sticky='nsew', padx=5, pady=5)
           setattr(self,f"Top_{index+1}", frame)

class divergesignal():
    def update_clock(self, app):
        self.t = time.localtime()
        self.current_time = time.strftime("%H:%M:%S", self.t)
        app.title ("Snooker Light Control software " + str(self.current_time))
    def runschedule(self):
        global shift_time
        self.t = time.localtime()
        self.current_time = time.strftime("%H:%M:%S", self.t)
        if self.current_time == shift_time:
            my_fileactions = fileactions()
            my_fileactions.create_file()            
    def update_tables(self, app):
        for index in range(12):
            myAttr = getattr(app, f"Top_{index+1}", "None")
            if myAttr.varBool.get()==True: myAttr.calc_time_used("none")
        #if app.Top_1.varBool.get() == True: app.Top_1.calc_time_used("None")
       
                
if __name__ == "__main__":
    

    
    #def cycle_relays():
        #mClass = checkboards()
        #my_win = logs.new_window()
        #for i in range(0,3):
            #mRet = mClass.check_board(i)
            #if mRet == True:
                #for j in range(1,5):
                    #mClass.set_relay(i , j, 1)
                    #time.sleep(1)
                    #mClass.set_relay(i, j, 0)
            #else:
                #messagebox.showinfo('Snooker Lighting Software', 'Error communicating with Board ' + str(i))
        


    
           
    def key_pressed(event):
        if event.keysym_num >= 65470 or event.keysym_num <=65481: index = event.keysym_num-65469 
        myAttr = getattr(app, f"Top_{index}", "None")
        myAttr.butt_pressed()
        
            
    app = main_view()
    app.bind("<Key>",key_pressed)
    width = app.winfo_screenwidth()
    height = app.winfo_screenheight()
    app.title ("Snooker Light Control software")
    app.minsize(width, height)
    menubar = MenuBar(app)
    app.config(menu=menubar)
    
    def signal_Handler(signum, frame):
        mClass = divergesignal()
        mClass.update_clock(app)
        mClass.update_tables(app)
        mClass.runschedule()
        
    signal.signal(signal.SIGALRM, signal_Handler)
    signal.setitimer(signal.ITIMER_REAL, 1, 1)    
    app.mainloop